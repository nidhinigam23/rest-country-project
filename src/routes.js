import { createMemoryHistory, createRouter } from 'vue-router';
import Layout from './components/CountryLayout.vue';
import Country from './components/CountryDetails.vue';
const routes = [
    {
        name: 'Layout',
        path: '/',
        component: Layout,
    },
    {
        name: 'Country',
        path: '/:code',
        component: Country,
    }
]


const router = createRouter({
    history: createMemoryHistory(),
    routes,
})

export default router;